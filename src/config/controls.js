import { Control, GamepadThumbstick } from "../constants/control.js";

export const controls = [
	// Player 1
	{
		gamePad: {
			[GamepadThumbstick.DEAD_ZONE]: 0.5,
			[GamepadThumbstick.HORIZONTAL_AXE_ID]: 0,
			[GamepadThumbstick.VERTICAL_AXE_ID]: 1,

			[Control.LEFT]: 14,
			[Control.RIGHT]: 15,
			[Control.UP]: 12,
			[Control.DOWN]: 13,
			[Control.LIGHT_PUNCH]: 2,
			[Control.MEDIUM_PUNCH]: 3,
			[Control.HEAVY_PUNCH]: 5,
			[Control.LIGHT_KICK]: 0,
			[Control.MEDIUM_KICK]: 1,
			[Control.HEAVY_KICK]: 4,
		},
		keyboard: {
			[Control.LEFT]: 'KeyA',
			[Control.RIGHT]: 'KeyD',
			[Control.UP]: 'KeyW',
			[Control.DOWN]: 'KeyS',
			[Control.LIGHT_PUNCH]: 'Digit1',
			[Control.MEDIUM_PUNCH]: 'Digit2',
			[Control.HEAVY_PUNCH]: 'Digit3',
			[Control.LIGHT_KICK]: 'Digit4',
			[Control.MEDIUM_KICK]: 'Digit5',
			[Control.HEAVY_KICK]: 'Digit6',
		},
	},

	// Player 2
	{
		gamePad: {
			[GamepadThumbstick.DEAD_ZONE]: 0.5,
			[GamepadThumbstick.HORIZONTAL_AXE_ID]: 0,
			[GamepadThumbstick.VERTICAL_AXE_ID]: 1,

			[Control.LEFT]: 14,
			[Control.RIGHT]: 15,
			[Control.UP]: 12,
			[Control.DOWN]: 13,
			[Control.LIGHT_PUNCH]: 2,
			[Control.MEDIUM_PUNCH]: 3,
			[Control.HEAVY_PUNCH]: 5,
			[Control.LIGHT_KICK]: 0,
			[Control.MEDIUM_KICK]: 1,
			[Control.HEAVY_KICK]: 4,
		},
		keyboard: {
			[Control.LEFT]: 'ArrowLeft',
			[Control.RIGHT]: 'ArrowRight',
			[Control.UP]: 'ArrowUp',
			[Control.DOWN]: 'ArrowDown',
			[Control.LIGHT_PUNCH]: 'KeyP',
			[Control.MEDIUM_PUNCH]: 'KeyO',
			[Control.HEAVY_PUNCH]: 'Digit0',
			[Control.LIGHT_KICK]: 'Digit9',
			[Control.MEDIUM_KICK]: 'Digit8',
			[Control.HEAVY_KICK]: 'Digit7',
		},
	},
];
